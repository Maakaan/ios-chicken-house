//
//  ChickenHouse.swift
//  Poulailler
//
//  Created by Ludovic Ollagnier on 29/03/2017.
//  Copyright © 2017 Ludovic Ollagnier. All rights reserved.
//

import Foundation

class ChickenHouse {
    
    enum ChickenHouseType {
        case small
        case medium
        case large
        
        static func allType() -> [ChickenHouseType] {
            return [.small, .medium, .large]
        }
    }
    
    static let instance = ChickenHouse()
    
    private var chickens: [Chicken] = []
    
    var type: ChickenHouseType = .medium
    
    static var maxLegalSize = 25
    
    func add(_ chicken: Chicken) {
        chickens.append(chicken)
        
        //Exemple de méthode dans un enum
        //ChickenHouseType.allType()
    }
    
    func remove(_ chicken: Chicken) {
        guard let index = chickens.index(of: chicken) else { return }
        print("coucou")
        removeChicken(at: index)
    }
    
    private func removeChicken(at index: Int) {
        chickens.remove(at: index)
    }
    
    var allChickens: [Chicken] {
        return chickens
    }
    
    func generateDemoData() {
        for i in 0...30 {
            let randomAge = Int(arc4random_uniform(5))
            let randomEggs = Int(arc4random_uniform(200))
            let p = Chicken(name: "Poulet \(i)", age: randomAge, owner: nil, eggLayed: randomEggs, isAlive: true)
            add(p)
        }
    }
}
