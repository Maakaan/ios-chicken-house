//
//  ChickenListTableViewController.swift
//  Poulailler
//
//  Created by Ludovic Ollagnier on 30/03/2017.
//  Copyright © 2017 Ludovic Ollagnier. All rights reserved.
//

import UIKit

class ChickenListTableViewController: UITableViewController {

    let house = ChickenHouse()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        house.generateDemoData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return house.allChickens.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier: String = "chickenCell"
        
        // Sort une cellule, du bon type
        guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? ChickenTableViewCell else { fatalError() }
        
        //Personnalisez la cellule avec les bonne infos
        let index = indexPath.row
        let poulet = house.allChickens[index]
        
        cell.ageLabel.text = "Age : \(poulet.age)"
        cell.nameLabel.text = "\(poulet.name)"
        cell.layedEggsLabel.text = "Oeufs pondus : \(poulet.eggLayed)"
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 44.0
    }
    

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showForm" {
            
            guard let form = segue.destination as? ViewController else {
                return
            }
            form.house = self.house

        } else if segue.identifier == "showDetails" {
            // Identifier la cellule sélectionnée
            // Récupérer l'objet correspondant
            // Passer le poulet à l'écran suivant
        }
    }

}
