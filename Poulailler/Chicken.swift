//
//  Chciken.swift
//  Poulailler
//
//  Created by Ludovic Ollagnier on 29/03/2017.
//  Copyright © 2017 Ludovic Ollagnier. All rights reserved.
//

protocol Volaille {
    
    var name: String { get }
    var age: Int { get set }
    var eggLayed: Int { get set }
    mutating func lay()
}

struct Turkey: Volaille {
    
    let name: String
    var age: Int
    var eggLayed: Int
    
   mutating func lay() {
        eggLayed += 1
    }
}

struct Chicken: Equatable, Volaille {
    
    let name: String
    var age: Int
    var owner: Owner?
    var eggLayed: Int
    var isAlive: Bool
    
    mutating func lay() {
        eggLayed += 1
    }
    
    public static func ==(lhs: Chicken, rhs: Chicken) -> Bool {
        
        if lhs.name == rhs.name && lhs.age == rhs.age && lhs.eggLayed == rhs.eggLayed {
            return true
        }
        
        return false
    }
}
