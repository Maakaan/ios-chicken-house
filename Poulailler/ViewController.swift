//
//  ViewController.swift
//  Poulailler
//
//  Created by Ludovic Ollagnier on 29/03/2017.
//  Copyright © 2017 Ludovic Ollagnier. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var ageSlider: UISlider!
    @IBOutlet weak var aliveSwitch: UISwitch!
    @IBOutlet weak var ownerNameTextField: UITextField!
    @IBOutlet weak var ageLabel: UILabel!
    
    var house = ChickenHouse()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func ageDidChange(_ sender: UISlider) {
        let age = Int(sender.value)
        ageLabel.text = String(age)
        ageLabel.text = "\(age)"
    }
    
    @IBAction func createChicken(_ sender: UIButton) {
        
        guard let name = nameTextField.text, name.characters.count >= 3 else { return }
//        guard let ageStr = nameTextField.text, let ageInt = Int(ageStr) else { return }
        let age = Int(ageSlider.value)
        let alive = aliveSwitch.isOn
        
        var owner: Owner?
        if let ownerName = ownerNameTextField.text, !ownerName.isEmpty {
            owner = Owner(name: ownerName)
        }

        let p = Chicken(name: name, age: age, owner: owner, eggLayed: 0, isAlive: alive)
        house.add(p)
        print(house.allChickens)
    }
}

