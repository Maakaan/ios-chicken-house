//
//  Owner.swift
//  Poulailler
//
//  Created by Ludovic Ollagnier on 29/03/2017.
//  Copyright © 2017 Ludovic Ollagnier. All rights reserved.
//

struct Owner {
    
    let name: String
}
